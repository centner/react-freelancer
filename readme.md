# React-freelancer
==========
**App build with:**
- react-router
- es6
- freelancer theme
- custom layout components & pages

**To run project locally:**
```
git clone https://github.com/alexandr-g/react-freelancer.git
cd react-freelancer
npm i
npm run dev
navigate to your http://localhost:8080/ in a browser
```
![screenshot 2016-08-18 14 32 08](https://cloud.githubusercontent.com/assets/9251327/17773827/9708ee38-6550-11e6-8e2b-a623bb65fbb9.png)

